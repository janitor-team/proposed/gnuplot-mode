(define-package
  "gnuplot-mode"
  "20141231"
  "Gnuplot mode for Emacs"
  nil
  :keywords
  '("gnuplot" "plotting")
  :authors
  '(("Bruce Ravel" . "bruceravel1@gmail.com")))
